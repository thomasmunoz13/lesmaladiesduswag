package fr.unice.polytech.flu;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Class part of the FLU project.
 *
 * @author Adrien Dengreville and Benjamin PIAT
 *         Created on 30/11/15
 **/

public abstract class Infectable {

    protected Map<Sickness, VirusHandler> virusHandlers;
    protected State state;

    protected enum State {
        HEALTHY,
        SICK,
        CONTAGIOUS,
        RECOVERING,
        DEAD
    }

    public Infectable() {
        virusHandlers = new HashMap<>();
    }

    protected abstract void infected(Virus virus);

    public abstract InfectableType whatAmI();

    public abstract boolean virusAttack(Virus virus, Infectable contagion);

    public void occur() {
        state = State.HEALTHY;
        virusHandlers.forEach((s, v) -> {
            v.iterate();

            switch (v.getState()) {
                case SICK:
                    if (state == State.HEALTHY) state = State.SICK;
                    break;
                case CONTAGIOUS:
                    if (state != State.DEAD) state = State.CONTAGIOUS;
                    break;
                case RECOVERING:
                    if (state == State.HEALTHY) state = State.RECOVERING;
                    break;
                case DEAD:
                    state = State.DEAD;
                    break;
            }
        });
    }

    public Collection getContamination() {
        return virusHandlers.values()
                .stream()
                .filter(i -> i.getState() == State.CONTAGIOUS)
                .map(VirusHandler::getVirus)
                .collect(Collectors.toCollection(ArrayList::new));
    }

    public List<Virus> getCurrentViruses() {
        List<Virus> viruses = new ArrayList<>();

        virusHandlers.entrySet().parallelStream().filter(v ->
                v.getValue().getState().equals(State.SICK) ||
                        v.getValue().getState().equals(State.CONTAGIOUS) ||
                        v.getValue().getState().equals(State.RECOVERING)
        ).forEach(v -> viruses.add(v.getValue().getVirus()));

        return viruses;
    }

    public State getState() {
        return state;
    }
}
