package fr.unice.polytech.flu;

import javax.swing.*;
import javax.swing.event.ChangeListener;
import java.awt.*;

/**
 * @author Thomas Munoz
 */
public class SliderView {

    // We use static here to declare a constant
    public final static int INIT_SLEEP = 500;

    private final static int MAX_SLEEP = 1000;
    private final static int MIN_SLEEP = 0;
    private final static String TITLE = "Flu controller";

    private JFrame frame;
    private JPanel panel;

    public SliderView(ChangeListener changeListener) {
        frame = new JFrame();
        panel = new JPanel();
        createFrame();
        frame.add(panel, BorderLayout.CENTER);
        createLabel();
        createSlider(changeListener);
        display();
    }

    private void createFrame() {
        frame.setTitle(TITLE);
        frame.setPreferredSize(new Dimension(250, 100));
        frame.setLocation(0, 400);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    private void createSlider(ChangeListener changeListener) {
        JSlider slider = new JSlider(SwingConstants.HORIZONTAL, MIN_SLEEP, MAX_SLEEP, INIT_SLEEP);
        slider.setMajorTickSpacing(100);
        slider.setMinorTickSpacing(1000);
        slider.setPaintTicks(true);
        slider.setPaintLabels(true);
        slider.setLabelTable(slider.createStandardLabels(500));
        slider.setBorder(BorderFactory.createEmptyBorder(0, 0, 10, 0));
        slider.addChangeListener(changeListener);

        panel.add(slider);
    }

    private void createLabel() {
        JLabel label = new JLabel("Sleep time");
        label.setAlignmentX(Component.CENTER_ALIGNMENT);
        label.setVisible(true);
        label.setSize(new Dimension(50, 100));

        panel.add(label);
    }

    private void display() {
        frame.pack();
        frame.setVisible(true);
    }
}
