package fr.unice.polytech.flu;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author Thomas MUNOZ
 *         Load the application config from a config file (config.properties)
 */
public class ApplicationConfigFileLoader {
    private Properties properties = new Properties();

    private void loadPropertiesFiles() {
        try (InputStream input = new FileInputStream("config.properties")) {
            properties.load(input);
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }

    public ApplicationConfig load() {
        ApplicationConfig config = new ApplicationConfig();
        loadPropertiesFiles();

        config.setHeight(Integer.parseInt(properties.getProperty("height")));
        config.setWidth(Integer.parseInt(properties.getProperty("width")));
        config.setInfectedRate(Integer.parseInt(properties.getProperty("infectedRate")));
        config.setNbAnimals(Integer.parseInt(properties.getProperty("nbAnimals")));
        config.setNbHumans(Integer.parseInt(properties.getProperty("nbHumans")));
        config.setNbTurns(Integer.parseInt(properties.getProperty("turns")));

        return config;
    }
}
