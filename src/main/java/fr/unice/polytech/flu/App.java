package fr.unice.polytech.flu;

import javax.swing.*;

/**
 * @author Thomas Munoz
 */
public class App {
    private Grid grid;
    private ApplicationConfig config;

    public App() {
        ApplicationConfigFileLoader configLoader = new ApplicationConfigFileLoader();
        config = configLoader.load();
        grid = new Grid(config);
    }

    public void run() throws InterruptedException {
        new SliderView(e -> {
            JSlider jSlider = (JSlider) e.getSource();
            grid.setSleepTime(jSlider.getValue());
        });

        grid.run(config.getNbTurns());
    }
}
