package fr.unice.polytech.flu;

/**
 * @author Thomas Munoz
 *         Represent a virus config0
 */
public class VirusConfig {
    private int infectRate;
    private int infectRange;
    private int deathRate;
    private int incubingTime;
    private int recoveringTime;
    private int sicknessTime;

    public int getInfectRate() {
        return infectRate;
    }

    public void setInfectRate(int infectRate) {
        this.infectRate = infectRate;
    }

    public int getInfectRange() {
        return infectRange;
    }

    public void setInfectRange(int infectRange) {
        this.infectRange = infectRange;
    }

    public int getDeathRate() {
        return deathRate;
    }

    public void setDeathRate(int deathRate) {
        this.deathRate = deathRate;
    }

    public int getIncubingTime() {
        return incubingTime;
    }

    public void setIncubingTime(int incubingTime) {
        this.incubingTime = incubingTime;
    }

    public int getRecoveringTime() {
        return recoveringTime;
    }

    public void setRecoveringTime(int recoveringTime) {
        this.recoveringTime = recoveringTime;
    }

    public int getSicknessTime() {
        return sicknessTime;
    }

    public void setSicknessTime(int sicknessTime) {
        this.sicknessTime = sicknessTime;
    }
}
