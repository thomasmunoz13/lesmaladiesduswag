package fr.unice.polytech.flu;

/**
 * Created by Benjamin PIAT on 30/11/15.
 *
 * @author Benjamin PIAT
 */
public class Virus {
    private float infectRate;
    private float deathRate;
    private Sickness sickness;
    private int incubingTime;
    private int recoveringTime;
    private int sicknessTime;


    public Virus(float infectRate, float deathRate, Sickness sickness, int incubingTime, int recoveringTime, int sicknessTime) {
        this.infectRate = infectRate;
        this.deathRate = deathRate;
        this.sickness = sickness;
        this.incubingTime = incubingTime;
        this.recoveringTime = recoveringTime;
        this.sicknessTime = sicknessTime;
    }

    public Virus(VirusConfig config, Sickness sickness) {
        infectRate = config.getInfectRate();
        deathRate = config.getDeathRate();
        this.sickness = sickness;
        incubingTime = config.getIncubingTime();
        recoveringTime = config.getRecoveringTime();
        sicknessTime = config.getSicknessTime();
    }

    public int getIncubingTime() {
        return incubingTime;
    }

    public float getInfectRate() {
        return infectRate;
    }

    public float getDeathRate() {
        return deathRate;
    }

    public Sickness getSickness() {
        return sickness;
    }

    public int getSicknessTime() {
        return sicknessTime;
    }

    public int getRecoveringTime() {
        return recoveringTime;
    }
}
