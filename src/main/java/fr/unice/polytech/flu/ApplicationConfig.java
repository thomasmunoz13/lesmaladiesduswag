package fr.unice.polytech.flu;

/**
 * @author Thomas Munoz
 *         Represent the application configuration
 */

public class ApplicationConfig {
    private int width;
    private int height;
    private int nbHumans;
    private int nbAnimals;
    private int infectedRate;
    private int nbTurns;

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getNbHumans() {
        return nbHumans;
    }

    public void setNbHumans(int nbHumans) {
        this.nbHumans = nbHumans;
    }

    public int getNbAnimals() {
        return nbAnimals;
    }

    public void setNbAnimals(int nbAnimals) {
        this.nbAnimals = nbAnimals;
    }

    public int getInfectedRate() {
        return infectedRate;
    }

    public void setInfectedRate(int infectedRate) {
        this.infectedRate = infectedRate;
    }

    public int getNbTurns() {
        return nbTurns;
    }

    public void setNbTurns(int nbTurns) {
        this.nbTurns = nbTurns;
    }
}
