package fr.unice.polytech.flu;

/**
 * Represent a cell in the grid
 *
 * @author Thomas Munoz
 */
public class Cell {
    protected Infectable owner;

    public Cell() {
        owner = null;
    }

    public Cell(Infectable owner) {
        this.owner = owner;
    }

    public Infectable getOwner() {
        return owner;
    }
}