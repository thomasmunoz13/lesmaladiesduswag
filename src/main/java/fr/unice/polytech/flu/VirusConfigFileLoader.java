package fr.unice.polytech.flu;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author Thomas Munoz
 *         Represent a virus config file loader (<virusname>.properties)
 */
public class VirusConfigFileLoader {
    private Properties properties;

    private void loadPropertiesFiles(Sickness sickness) {
        properties = new Properties();
        try (InputStream input = new FileInputStream(sickness.getName().toLowerCase() + ".properties")) {
            properties.load(input);
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }

    public VirusConfig load(Sickness sickness) {
        VirusConfig virusConfig = new VirusConfig();
        loadPropertiesFiles(sickness);

        virusConfig.setDeathRate(Integer.parseInt(properties.getProperty("deathRate")));
        virusConfig.setIncubingTime(Integer.parseInt(properties.getProperty("incubingTime")));
        virusConfig.setInfectRange(Integer.parseInt(properties.getProperty("infectRange")));
        virusConfig.setInfectRate(Integer.parseInt(properties.getProperty("infectRate")));
        virusConfig.setRecoveringTime(Integer.parseInt(properties.getProperty("recoveringTime")));
        virusConfig.setSicknessTime(Integer.parseInt(properties.getProperty("sicknessTime")));

        return virusConfig;
    }
}
