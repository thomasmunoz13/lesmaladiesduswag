package fr.unice.polytech.flu;

import java.util.function.Consumer;

/**
 * Represent the field (people and animals in the grid)
 * Inspired by the fox and rabbit Field class (we simplified it and made a few changes)
 *
 * @author Thomas Munoz
 */

public class Field {

    private Infectable[][] elements;

    private int width;
    private int height;

    public Field(int height, int width) {
        this.height = height;
        this.width = width;
        elements = new Infectable[height][width];
    }

    public void place(Infectable infectable, int x, int y) {
        elements[y][x] = infectable;
    }

    public Infectable getObjectAt(int x, int y) {
        if (x < 0 || y < 0) {
            return null;
        }

        return elements[y][x];
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void forEach(Consumer<? super Infectable> action) {
        for (int i = 0; i < height; ++i) {
            for (int j = 0; j < width; ++j) {
                if (elements[i][j] != null) {
                    action.accept(elements[i][j]);
                }
            }
        }
    }
}
