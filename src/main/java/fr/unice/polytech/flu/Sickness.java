package fr.unice.polytech.flu;

/**
 * @author Benjamin PIAT
 */

//Represent sicknesses
public enum Sickness {
    H1N1("H1N1"),
    H5N1("H5N1");

    private String name;

    Sickness(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
