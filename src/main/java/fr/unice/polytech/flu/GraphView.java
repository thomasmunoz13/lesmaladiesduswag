package fr.unice.polytech.flu;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;

/**
 * The GraphView provides a view of two populations of actors in the field as a
 * line graph over time. In its current version, it can only plot exactly two
 * different classes of animals. If further animals are introduced, they will
 * not currently be displayed.
 *
 * @author Michael Kölling and David J. Barnes and Adrien DENGREVILLE
 * @version 2011.07.31
 */
public class GraphView implements SimulatorView {
    private static final Color RED = new Color(198, 40, 40);
    private static final Color GREEN = new Color(56, 142, 60, 50);
    private static final Color PINK = new Color(255, 145, 194, 50);

    private static JFrame frame;
    private static GraphPanel graph;
    private static JLabel stepLabel;
    private static JLabel countLabel;

    // A map for storing colors for participants in the simulation
    private Map<Class, Color> colors;
    // A statistics object computing and storing simulation information
    private GridStats stats;

    /**
     * Constructor.
     *
     * @param width    The width of the plotter window (in pixles).
     * @param height   The height of the plotter window (in pixles).
     * @param startMax The initial maximum value for the y axis.
     */
    public GraphView(int width, int height, int startMax) {
        stats = new GridStats();
        colors = new HashMap<>();

        if (frame == null) {
            frame = makeFrame(width, height, startMax);
        } else {
            graph.newRun();
        }
    }

    /**
     * Define a color to be used for a given class of animal.
     *
     * @param animalClass The animal's Class object.
     * @param color       The color to be used for the given class.
     */
    public void setColor(Class animalClass, Color color) {
        colors.put(animalClass, color);
    }

    /**
     * Show the current status of the field. The status is shown by displaying a
     * line graph for two classes in the field. This view currently does not
     * work for more (or fewer) than exactly two classes. If the field contains
     * more than two different types of animal, only two of the classes will be
     * plotted.
     *
     * @param step  Which iteration step it is.
     * @param field The field whose status is to be displayed.
     */
    public void showStatus(int step, Field field) {
        stats.update(field);
        graph.update(step, stats);
    }

    /**
     * Prepare for a new run.
     */
    public void reset() {
        graph.newRun();
    }

    /**
     * Prepare the frame for the graph display.
     */
    private JFrame makeFrame(int width, int height, int startMax) {
        JFrame frame = new JFrame("Graph View");
        frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);

        Container contentPane = frame.getContentPane();

        graph = new GraphPanel(width, height, startMax);
        contentPane.add(graph, BorderLayout.CENTER);

        JPanel bottom = new JPanel();
        bottom.add(new JLabel("Step:"));
        stepLabel = new JLabel("");
        bottom.add(stepLabel);
        countLabel = new JLabel(" ");
        bottom.add(countLabel);
        contentPane.add(bottom, BorderLayout.SOUTH);

        frame.pack();
        frame.setLocation(20, 600);

        frame.setVisible(true);

        return frame;
    }

    public void increaseDeathToll() {
        stats.addDeath();
    }

    // ============================================================================

    /**
     * Nested class: a component to display the graph.
     */
    class GraphPanel extends JComponent {
        private static final double SCALE_FACTOR = 0.8;

        // An internal image buffer that is used for painting. For
        // actual display, this image buffer is then copied to screen.
        private BufferedImage graphImage;
        private int lastVal1, lastVal2, lastVal3;
        private int yMax;

        /**
         * Create a new, empty GraphPanel.
         */
        public GraphPanel(int width, int height, int startMax) {
            graphImage = new BufferedImage(width, height,
                    BufferedImage.TYPE_INT_RGB);
            clearImage();
            lastVal1 = height;
            lastVal2 = height;
            lastVal3 = height;
            yMax = startMax;
        }

        /**
         * Indicate a new simulation run on this panel.
         */
        public void newRun() {
            int height = graphImage.getHeight();
            int width = graphImage.getWidth();

            Graphics g = graphImage.getGraphics();
            g.copyArea(4, 0, width - 4, height, -4, 0);
            g.setColor(Color.BLACK);
            g.drawLine(width - 4, 0, width - 4, height);
            g.drawLine(width - 2, 0, width - 2, height);
            lastVal1 = height;
            lastVal2 = height;
            lastVal3 = height;
            repaint();
        }

        /**
         * Dispay a new point of data.
         */
        public void update(int step, GridStats stats) {
            int humans = stats.getHumans();
            int deaths = stats.getDeathToll();
            int animals = stats.getAnimals();

            Graphics g = graphImage.getGraphics();

            int height = graphImage.getHeight();
            int width = graphImage.getWidth();

            // move graph one pixel to left
            g.copyArea(1, 0, width - 1, height, -1, 0);

            // calculate y, check whether it's out of screen. scale down if
            // necessary.

            //to display the humans-----------------------
            int y = height - ((height * humans) / yMax) - 1;
            while (y < 0) {
                scaleDown();
                y = height - ((height * humans) / yMax) - 1;
            }
            g.setColor(PINK);
            g.drawLine(width - 2, y, width - 2, height);
            g.drawLine(width - 3, lastVal1, width - 2, y);
            lastVal1 = y;

            //to display the deaths-----------------------
            y = height - ((height * deaths) / yMax) - 1;
            while (y < 0) {
                scaleDown();
                y = height - ((height * deaths) / yMax) - 1;
            }
            g.setColor(RED);
            g.drawLine(width - 2, y, width - 2, height);
            g.drawLine(width - 3, lastVal2, width - 2, y);
            lastVal2 = y;

            //to display the animals----------------------
            y = height - ((height * animals) / yMax) - 1;
            while (y < 0) {
                scaleDown();
                y = height - ((height * animals) / yMax) - 1;
            }
            g.setColor(GREEN);
            g.drawLine(width - 2, y, width - 2, height);
            g.drawLine(width - 3, lastVal3, width - 2, y);
            lastVal3 = y;

            //i want to refactor this so badly
            repaintNow();

            stepLabel.setText("" + step);
            countLabel.setText("Red : Death toll (" + deaths + ") " +
                    "| Green : Animal population (" + animals + ") " +
                    "| Pink : Human population (" + humans + ")");
            // }
        }

        /**
         * Scale the current graph down vertically to make more room at the top.
         */
        public void scaleDown() {
            Graphics g = graphImage.getGraphics();
            int height = graphImage.getHeight();
            int width = graphImage.getWidth();

            BufferedImage tmpImage = new BufferedImage(width,
                    (int) (height * SCALE_FACTOR), BufferedImage.TYPE_INT_RGB);
            Graphics2D gtmp = (Graphics2D) tmpImage.getGraphics();

            gtmp.scale(1.0, SCALE_FACTOR);
            gtmp.drawImage(graphImage, 0, 0, null);

            int oldTop = (int) (height * (1.0 - SCALE_FACTOR));

            g.setColor(Color.WHITE);
            g.fillRect(0, 0, width, oldTop);
            g.drawImage(tmpImage, 0, oldTop, null);

            yMax = (int) (yMax / SCALE_FACTOR);
            lastVal1 = oldTop + (int) (lastVal1 * SCALE_FACTOR);
            lastVal2 = oldTop + (int) (lastVal2 * SCALE_FACTOR);
            lastVal3 = oldTop + (int) (lastVal3 * SCALE_FACTOR);

            repaint();
        }

        /**
         * Cause immediate update of the panel.
         */
        public void repaintNow() {
            paintImmediately(0, 0, graphImage.getWidth(),
                    graphImage.getHeight());
        }

        /**
         * Clear the image on this panel.
         */
        public void clearImage() {
            Graphics g = graphImage.getGraphics();
            g.setColor(Color.WHITE);
            g.fillRect(0, 0, graphImage.getWidth(), graphImage.getHeight());
            repaint();
        }

        // The following methods are redefinitions of methods
        // inherited from superclasses.

        /**
         * Tell the layout manager how big we would like to be. (This method
         * gets called by layout managers for placing the components.)
         *
         * @return The preferred dimension for this component.
         */
        public Dimension getPreferredSize() {
            return new Dimension(graphImage.getWidth(), graphImage.getHeight());
        }

        /**
         * This component is opaque.
         */
        public boolean isOpaque() {
            return true;
        }

        /**
         * This component needs to be redisplayed. Copy the internal image to
         * screen. (This method gets called by the Swing screen painter every
         * time it wants this component displayed.)
         *
         * @param g The graphics context that can be used to draw on this
         *          component.
         */
        public void paintComponent(Graphics g) {
            if (graphImage != null) {
                g.drawImage(graphImage, 0, 0, null);
            }
        }
    }
}
