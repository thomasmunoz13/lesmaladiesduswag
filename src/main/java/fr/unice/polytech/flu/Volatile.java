package fr.unice.polytech.flu;

import java.util.Random;

/**
 * Class part of the FLU project.
 *
 * @author Adrien Dengreville and Benjamin PIAT
 *         Created on 30/11/15
 **/

public class Volatile extends Animal {

    public Volatile(int proba, Virus virus) {
        super();
        if (proba < Randomizer.getRandom().nextInt(101)) state = State.HEALTHY;
        else {
            state = State.SICK;
            infected(virus);
        }
    }

    @Override
    public boolean virusAttack(Virus virus, Infectable contagion) {

        boolean becomeSick = false;
        Random rand = new Random();
        if (virus.getSickness() == Sickness.H5N1 && !(contagion instanceof Person)) {
            if (rand.nextInt(101) < virus.getInfectRate()) {
                infected(virus);
                becomeSick = true;
            }
        }
        return becomeSick;
    }
}
