package fr.unice.polytech.flu;

/**
 * @author Adrien Dengreville
 */
public enum InfectableType {
    PERSON,
    ANIMAL
}
