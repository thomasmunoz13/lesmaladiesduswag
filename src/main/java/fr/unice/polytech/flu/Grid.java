package fr.unice.polytech.flu;

import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * Represent the Grid and the behaviour of it
 *
 * @author Benjamin PIAT, Thomas MUNOZ, Adrien DENGREVILLE
 */
public class Grid {
    private SimulatorView gridView;
    private GraphView gridGraph;
    private Field field;
    private int sleepTime;
    private VirusContext virusContext;
    private Random random;


    public Grid(ApplicationConfig config) {
        int width = config.getWidth();
        int height = config.getHeight();
        int nbHumans = config.getNbHumans();
        int nbAnimals = config.getNbAnimals();
        this.random = new Random();
        this.sleepTime = SliderView.INIT_SLEEP;

        field = new Field(height, width);

        virusContext = new VirusContext();

        buildGrid(width, height, nbHumans, nbAnimals, config.getInfectedRate());

        gridGraph = new GraphView(500, 500, (nbHumans + nbAnimals));

        gridView = new GridView(height, width);
        gridView.setColor(Person.class, Color.BLUE);
        gridView.setColor(Pig.class, Color.PINK);
        gridView.setColor(Duck.class, Color.GREEN);
        gridView.setColor(Chicken.class, Color.YELLOW);

    }

    private void buildGrid(int width, int height, int nbHumans, int nbAnimals, int average) {
        List<Cell> cells = new ArrayList<>();
        for (int i = 0; i < (height * width) - (nbHumans + nbAnimals); i++) cells.add(new Cell());
        for (int i = 0; i < nbHumans; i++) cells.add(new Cell(new Person()));
        for (int i = 0; i < nbAnimals; i++) cells.add(new Cell(randomAnimal(average)));

        Collections.shuffle(cells);

        for (int i = 0; i < cells.size(); i++) {
            field.place(cells.get(i).getOwner(), i / width, i % width);
        }
    }

    public void run(int steps) throws InterruptedException {
        for (int i = 0; i < steps; ++i) {
            fieldBrowse();
            Thread.sleep(sleepTime);
            gridGraph.showStatus(i, field);
            gridView.showStatus(i, field);
        }
    }

    public void setSleepTime(int time) {
        sleepTime = time;
    }


    private void fieldBrowse() {

        for (int i = 0; i < field.getHeight(); i++) {
            for (int j = 0; j < field.getWidth(); j++) {
                if (field.getObjectAt(i, j) != null) {
                    field.getObjectAt(i, j).occur();
                    if (field.getObjectAt(i, j).getState() != Infectable.State.DEAD) {
                        Collection virus = field.getObjectAt(i, j).getContamination();
                        if (!virus.isEmpty()) spread(i, j, field.getObjectAt(i, j), virus);
                        move(i, j);
                    } else {
                        gridGraph.increaseDeathToll();
                        field.place(null, i, j);
                    }
                }
            }
        }
    }

    private void move(int x, int y) {
        int randomMove = random.nextInt(4);
        try {
            switch (randomMove) {
                case 0:
                    if (field.getObjectAt(x - 1, y) == null) {
                        field.place(field.getObjectAt(x, y), x - 1, y);
                        field.place(null, x, y);
                    }
                    break;
                case 1:
                    if (field.getObjectAt(x, y - 1) == null) {
                        field.place(field.getObjectAt(x, y), x, y - 1);
                        field.place(null, x, y);
                    }
                    break;
                case 2:
                    if (field.getObjectAt(x + 1, y) == null) {
                        field.place(field.getObjectAt(x, y), x + 1, y);
                        field.place(null, x, y);
                    }
                    break;
                case 3:
                    if (field.getObjectAt(x, y + 1) == null) {
                        field.place(field.getObjectAt(x, y), x, y + 1);
                        field.place(null, x, y);
                    }
                    break;
            }
        } catch (ArrayIndexOutOfBoundsException e) {

        }
    }

    private void spread(int x, int y, Infectable source, Collection<Virus> virus) {
        try {
            if (x - 1 > 0) virus.forEach(v -> field.getObjectAt(x - 1, y).virusAttack(v, source));
        } catch (NullPointerException e) {
        }
        try {
            if (y - 1 > 0) virus.forEach(v -> field.getObjectAt(x, y - 1).virusAttack(v, source));
        } catch (NullPointerException e) {
        }
        try {
            if (x + 1 < field.getHeight()) virus.forEach(v -> field.getObjectAt(x + 1, y).virusAttack(v, source));
        } catch (NullPointerException e) {
        }
        try {
            if (y + 1 < field.getWidth()) virus.forEach(v -> field.getObjectAt(x, y + 1).virusAttack(v, source));
        } catch (NullPointerException e) {
        }
    }

    private Animal randomAnimal(int averageContamination) {
        int nbAnimals = AvailableAnimals.values().length;
        AvailableAnimals result = AvailableAnimals.values()[random.nextInt(nbAnimals)];

        switch (result) {
            case PIG:
                return new Pig(averageContamination, virusContext.getVirus(Sickness.H1N1));
            case CHICKEN:
                return new Chicken(averageContamination, virusContext.getVirus(Sickness.H5N1));
            case DUCK:
                return new Duck(averageContamination, virusContext.getVirus(Sickness.H5N1));
            default:
                throw new RuntimeException();
        }
    }
}
