package fr.unice.polytech.flu;

/**
 * @author Benjamin PIAT
 */
public class VirusHandler {
    private Virus virus;
    private Infectable.State state;
    private int Days;
    private boolean isAnimal;


    public VirusHandler(Virus virus, boolean isAnimal) {
        this.virus = virus;
        this.state = Infectable.State.SICK;
        this.Days = 0;
        this.isAnimal = isAnimal;
    }

    public void iterate() {

        if (Days >= virus.getIncubingTime() && state == Infectable.State.SICK) {
            state = Infectable.State.CONTAGIOUS;
            Days = 0;
        }

        if (isAnimal) {
            if (state == Infectable.State.CONTAGIOUS && Days >= virus.getSicknessTime()
                    && Randomizer.getRandom().nextInt(101) < virus.getDeathRate()) {
                state = Infectable.State.DEAD;
            }
        } else {

            if (state == Infectable.State.CONTAGIOUS && Days >= virus.getSicknessTime()) {
                if (Randomizer.getRandom().nextInt(101) < virus.getDeathRate()) {
                    state = Infectable.State.DEAD;
                } else {
                    state = Infectable.State.RECOVERING;
                }
                Days = 0;
            }
            if (state == Infectable.State.RECOVERING && Days >= virus.getRecoveringTime()) {
                state = Infectable.State.HEALTHY;
                Days = 0;
            }
        }
        ++Days;
    }

    public Infectable.State getState() {
        return state;
    }

    public Virus getVirus() {
        return virus;
    }
}
