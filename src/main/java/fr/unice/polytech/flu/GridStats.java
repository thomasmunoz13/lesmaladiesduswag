package fr.unice.polytech.flu;

/**
 * @author Adrien DENGREVILLE on 14/12/15
 */
public class GridStats {
    private int humans;
    private int animals;
    private int deathToll;

    public GridStats() {
        humans = 0;
        animals = 0;
        deathToll = 0;
    }

    public void update(Field field) {
        //get human count
        humans = animals = 0;

        field.forEach(i -> {
            switch (i.whatAmI()) {
                case PERSON:
                    humans += 1;
                    break;
                case ANIMAL:
                    animals += 1;
                    break;
            }
        });
    }

    public void addDeath() {
        deathToll += 1;
    }

    public int getHumans() {
        return humans;
    }

    public int getAnimals() {
        return animals;
    }

    public int getDeathToll() {
        return deathToll;
    }
}
