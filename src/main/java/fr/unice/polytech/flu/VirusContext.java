package fr.unice.polytech.flu;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Benjamin Piat
 * @author Thomas Munoz
 */
public class VirusContext {
    Map<Sickness, Virus> currentVirus = new HashMap<>();

    public VirusContext() {
        VirusConfigFileLoader virusConfigFileLoader = new VirusConfigFileLoader();

        addVirus(Sickness.H1N1, virusConfigFileLoader.load(Sickness.H1N1));
        addVirus(Sickness.H5N1, virusConfigFileLoader.load(Sickness.H5N1));
    }

    public void addVirus(Sickness sickness, VirusConfig config) {
        currentVirus.put(sickness, new Virus(config, sickness));
    }

    public Virus getVirus(Sickness sickness) {
        return currentVirus.get(sickness);
    }
}
