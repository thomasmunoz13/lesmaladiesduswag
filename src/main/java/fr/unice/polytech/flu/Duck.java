package fr.unice.polytech.flu;

/**
 * Class part of the FLU project.
 *
 * @author Adrien Dengreville
 *         Created on 30/11/15
 **/

public class Duck extends Volatile {
    public Duck(int proba, Virus virus) {
        super(proba, virus);
    }
}
