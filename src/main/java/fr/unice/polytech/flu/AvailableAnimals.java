package fr.unice.polytech.flu;

/**
 * Class part of the FLU project.
 *
 * @author Adrien Dengreville
 *         Created on 01/12/15
 **/

public enum AvailableAnimals {
    PIG,
    CHICKEN,
    DUCK
}
