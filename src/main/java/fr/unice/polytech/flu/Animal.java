package fr.unice.polytech.flu;

/**
 * Class part of the FLU project.
 *
 * @author Adrien Dengreville
 *         Created on 30/11/15
 **/

public abstract class Animal extends Infectable {

    protected void infected(Virus virus) {
        VirusHandler handler = new VirusHandler(virus, true);

        virusHandlers.put(virus.getSickness(), handler);

        if (state != State.CONTAGIOUS)
            state = State.SICK;
    }

    @Override
    public InfectableType whatAmI() {
        return InfectableType.ANIMAL;
    }
}
