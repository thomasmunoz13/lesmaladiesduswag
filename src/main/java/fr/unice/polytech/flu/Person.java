package fr.unice.polytech.flu;

import java.util.Random;

/**
 * Class part of the FLU project.
 *
 * @author Adrien Dengreville and Benjamin PIAT
 *         Created on 30/11/15
 **/

public class Person extends Infectable {

    public Person() {
        super();
        state = State.HEALTHY;
    }

    @Override
    protected void infected(Virus virus) {
        VirusHandler handler = new VirusHandler(virus, false);
        virusHandlers.put(virus.getSickness(), handler);
        if (state != State.CONTAGIOUS) state = State.SICK;
    }

    @Override
    public InfectableType whatAmI() {
        return InfectableType.PERSON;
    }

    @Override
    public boolean virusAttack(Virus virus, Infectable contagion) {

        boolean becomeSick = false;
        Random rand = new Random();

        if (contagion instanceof Person) {
            if (rand.nextInt(101) < virus.getInfectRate()) {
                infected(virus);
                becomeSick = true;
            }
        } else {
            if (rand.nextInt(101) < virus.getInfectRate() / 2) {
                infected(virus);
                becomeSick = true;
            }
        }

        return becomeSick;
    }
}
