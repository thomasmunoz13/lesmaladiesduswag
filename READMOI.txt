[![Build Status](http://ci.munoz.ovh/buildStatus/icon?job=Flu)](https://ci.munoz.ovh/job/Flu)
# Flu project
Authors : Adrien Dengreville, Benjamin Piat, Thomas Munoz

## To run the application, you just have to run the run.sh script, like this :

```
./run.sh
```

### This script is just a shortcut to 

```
mvn package
java -jar target/flu.jar
```
